import Trucknutz as TS


def fill_buffer():
    """ Fills the buffer with boxes using the get_next_box function """
    for i in range(len(buffer_array)):
        """ If an entry in the buffer is not filled, it will be filled with a new box"""
        if buffer_array[i] == 0:
            buffer_array[i] = TS.get_next_box()

    return buffer_array


def check_buffer_for(box_id):
    """ Checks the buffer for a certain box_id and returns a list of
    the positions where its placed in the buffer array """
    buffer_placement = []

    for i in range(len(buffer_array)):
        if buffer_array[i][0] == box_id:
            buffer_placement.append(i)

    return buffer_placement


def take_box_from_buffer(buffer_array_id):
    """ Takes a box from the buffer using the box placement in the buffer array
    and replaces the now empty space with a 0 """

    if buffer_array_id in [0, 1, 2, 3, 4]:
        box = buffer_array[buffer_array_id]
        buffer_array[buffer_array_id] = 0
        fill_buffer()

        return box

    else:
        return 0


def get_box(box_id):
    """ Takes a box from the buffer using the box_id and fills the buffer afterwards"""

    box_placement = check_buffer_for(box_id)
    if len(box_placement) < 1:
        return 0

    else:
        box = take_box_from_buffer(box_placement[0])
        return box


def setup():
    """ Setup is run in the start to declare the different global variables,
     set the truck size and fill the buffer array with boxes"""
    global truck_width
    global truck_height
    global buffer_array
    global row_number

    truck_width = 25
    truck_height = 25
    buffer_array = [0, 0, 0, 0, 0]
    row_number = 0

    TS.set_truck_size(truck_width, truck_height)

    fill_buffer()


def main():
    setup()
    fill_row(0)


def fill_row(row_index):
    column_index = 0
    free_space = 0

    while not is_row_full(row_index):
        free_space = TS.get_free_in_row(0)

        if get_box(1) != 0:
            box = get_box(1)
            TS.add_box(box, column_index, row_index, 0)
            column_index += 3

            TS.plot_truck()




def is_row_full(row_index):
    TS.update_truck()
    free_space = TS.get_free_in_row(row_index)

    if len(free_space) == 0:
        return True
    else:
        return False


main()
