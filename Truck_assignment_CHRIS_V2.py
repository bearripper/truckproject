import Trucknutz as TS


def main():
    setup()
    fill_truck()


def setup():
    """ Setup is run in the start to declare the different global variables,
     set the truck size and fill the buffer array with boxes"""
    global truck_width
    global truck_height
    global buffer_array
    global row_number

    truck_width = 25
    truck_height = 25
    buffer_array = [0, 0, 0, 0, 0]

    get_box(0, 1)

    TS.set_truck_size(truck_width, truck_height)


def get_box(box_id, only_check):

    if buffer_array == [0, 0, 0, 0, 0]:
        for i in range(5):
            buffer_array[i] = TS.get_next_box()

    else:

        box = 0

        for i in range(5):
            if buffer_array[i][0] == box_id:
                box = buffer_array[i][0]

                if only_check == 0:
                    box = buffer_array[i]
                    buffer_array[i] = TS.get_next_box()

                break

        return box


def next_box():
    most_boxes = [0, 0, 0, 0]
    for i in range(5):
        for j in range(4):
            if buffer_array[i][0] == j + 1:
                most_boxes[j] += 1

    most_boxes_index = 0
    i_index = 0
    for i in range(4):
        if most_boxes[i] > most_boxes_index:
            most_boxes_index = most_boxes[i]
            i_index = i

    return i_index + 1


def fill_truck():
    column_index = 0
    row_index = 0
    free_space = 0

    for k in range(100):
        free_space = TS.get_free_in_row(row_index)

        print(free_space, "Free space")
        if len(free_space) < 8:
            if len(free_space) != 0:
                if free_space[-1] == truck_width - 1:
                    arranged_boxes = arrange_box_to_length(len(free_space), 1)
                else:
                    arranged_boxes = arrange_box_to_length(len(free_space), 0)

            print(arranged_boxes, "Arranged boxes")

            for i in range(len(arranged_boxes)):
                j = ((i + 1) * -1)

                if arranged_boxes[j] == 4:
                    box = get_box(4, 0)
                    TS.add_box(box, column_index, row_index, 1)
                    column_index += 3

                elif arranged_boxes[j] == 3:
                    box = get_box(3, 0)
                    TS.add_box(box, column_index, row_index, 1)
                    box = get_box(3, 0)
                    TS.add_box(box, column_index, row_index + 1, 1)
                    column_index += 3

                elif arranged_boxes[j] == 2:
                    box = get_box(2, 0)
                    TS.add_box(box, column_index, row_index, 0)
                    column_index += 2

                elif arranged_boxes[j] == 1:
                    box = get_box(1, 0)
                    TS.add_box(box, column_index, row_index, 0)
                    column_index += 3

            # TS.plot_truck()
            row_index += 2
            column_index = 0
        else:

            if next_box() == 3:
                box = get_box(3, 0)
                TS.add_box(box, column_index, row_index, 1)
                box = get_box(3, 0)
                TS.add_box(box, column_index, row_index + 1, 1)
                column_index += 3

            elif get_box(1, 1) == 1:
                box = get_box(1, 0)
                TS.add_box(box, column_index, row_index, 0)
                column_index += 3

            elif get_box(2, 1) == 2:
                box = get_box(2, 0)
                TS.add_box(box, column_index, row_index, 0)
                column_index += 2
    TS.plot_truck()

def arrange_box_to_length(length, full_lenght):

    boxes_to_place = []
    print(buffer_array, "bufferArray")
    prev_box = 0
    box_3_used = 0

    if get_box(4, 1) == 4 and full_lenght:
        boxes_to_place.append(4)
        length -= 3
        prev_box = 4
    elif check_buffer_amount_for(3) == 2:
        boxes_to_place.append(3)
        length -= 3
        prev_box = 3
        box_3_used = 2
    elif get_box(1, 1) == 1 and prev_box != 1:
        boxes_to_place.append(1)
        length -= 3
        prev_box = 1
    elif get_box(2, 1) == 2 and prev_box != 2:
        boxes_to_place.append(2)
        length -= 2
        prev_box = 2

    if length == 5:
        if get_box(2, 1) == 2:
            boxes_to_place.append(2)
        if get_box(1, 1) == 1:
            boxes_to_place.append(1)
        elif check_buffer_amount_for(3) == 2:
            boxes_to_place.append(3)
    elif length == 4:
        if get_box(2, 1) == 2:
            boxes_to_place.append(2)
            boxes_to_place.append(2)
    elif length == 3:
        if get_box(1, 1) == 1:
            boxes_to_place.append(1)
        elif check_buffer_amount_for(3) == 2 + box_3_used:
            boxes_to_place.append(3)
    elif length == 2:
        if get_box(2, 1) == 2:
            boxes_to_place.append(2)
    return boxes_to_place


def check_buffer_amount_for(box_id):
    amount = 0

    for i in range(len(buffer_array)):
        if buffer_array[i][0] == box_id:
            amount += 1

    return amount


main()
